﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AnimeCheck.View
{
    public partial class WatchedAnimePage : Page
    {
        public WatchedAnimePage()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            DataProcessing.ReadFile();
        }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            List<Title> titles = new List<Title>();
            foreach (var title in DataProcessing.TitleList)
            {
                if (title.WatchedParts.Count > 0)
                    titles.Add(title);
            }
            AnimeList.ItemsSource = titles;
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
