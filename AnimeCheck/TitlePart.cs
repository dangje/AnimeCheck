﻿

using System.Collections.Generic;

namespace AnimeCheck
{
    public class TitlePart
    {
        public string PartName {get; set;}
        public TitlePart(string name)
        {
            PartName = name;
        }
    }
}